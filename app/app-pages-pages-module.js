(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-pages-pages-module"],{

/***/ "./src/app/pages/api/api.component.ts":
/*!********************************************!*\
  !*** ./src/app/pages/api/api.component.ts ***!
  \********************************************/
/*! exports provided: ApiComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiComponent", function() { return ApiComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_input_theme_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/input-theme.service */ "./src/app/services/input-theme.service.ts");
/* harmony import */ var _services_scpc_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/scpc.service */ "./src/app/services/scpc.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ApiComponent = /** @class */ (function () {
    function ApiComponent(inputservice, scpcservice) {
        this.inputservice = inputservice;
        this.scpcservice = scpcservice;
    }
    ApiComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-api',
            template: __webpack_require__(/*! ./api.html */ "./src/app/pages/api/api.html"),
            styles: [__webpack_require__(/*! ./api.scss */ "./src/app/pages/api/api.scss")],
        }),
        __metadata("design:paramtypes", [_services_input_theme_service__WEBPACK_IMPORTED_MODULE_1__["InputThemeService"], _services_scpc_service__WEBPACK_IMPORTED_MODULE_2__["SCPCService"]])
    ], ApiComponent);
    return ApiComponent;
}());



/***/ }),

/***/ "./src/app/pages/api/api.html":
/*!************************************!*\
  !*** ./src/app/pages/api/api.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n\t<div class=\"col-md-6 col-xxl-6 col-sm-12 mh-75 h-75\">\n\t\t<nb-card> <nb-card-header>SCPC API endpoint</nb-card-header>\n\t\t<nb-card-body>\n\t\t<div class=\"form-group row\">\n\t\t\t<label for=\"inputapiURL\" class=\"col-sm-3 col-form-label\">API&nbsp;endpoint&nbsp;URL</label>\n\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t<input type=\"text\"\n\t\t\t\t\tclass=\"form-control {{scpcservice.isApiAvailable?'form-control-success':'form-control-danger'}}\"\n\t\t\t\t\t(change)=\"scpcservice.getData()\" [(ngModel)]=\"scpcservice.apiURL\" placeholder=\"http://...\">\n\t\t\t</div>\n\t\t</div>\n\t\t<div>\n\t\t\t<pre>{{scpcservice.lastResponse}}</pre>\n\t\t</div>\n\t\t</nb-card-body> </nb-card>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/pages/api/api.module.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/api/api.module.ts ***!
  \*****************************************/
/*! exports provided: ApiModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiModule", function() { return ApiModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _api_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.component */ "./src/app/pages/api/api.component.ts");
/* harmony import */ var _services_input_theme_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/input-theme.service */ "./src/app/services/input-theme.service.ts");
/* harmony import */ var _services_scpc_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/scpc.service */ "./src/app/services/scpc.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ApiModule = /** @class */ (function () {
    function ApiModule() {
    }
    ApiModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__["ThemeModule"],
            ],
            declarations: [
                _api_component__WEBPACK_IMPORTED_MODULE_2__["ApiComponent"],
            ],
            providers: [_services_input_theme_service__WEBPACK_IMPORTED_MODULE_3__["InputThemeService"], _services_scpc_service__WEBPACK_IMPORTED_MODULE_4__["SCPCService"]],
        })
    ], ApiModule);
    return ApiModule;
}());



/***/ }),

/***/ "./src/app/pages/api/api.scss":
/*!************************************!*\
  !*** ./src/app/pages/api/api.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "pre {\n  color: white;\n  overflow-y: auto; }\n"

/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.component.html":
/*!**********************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n<div class=\"col-md-6 col-xxl-6 col-sm-12\">\n\t<table class=\"table\">\n\t\t<thead class=\"thead-dark\">\n\t\t\t<tr>\n\t\t\t\t<th scope=\"col\">Name</th>\n\t\t\t\t<th scope=\"col\">Value</th>\n\t\t\t</tr>\n\t\t</thead>\n\t\t<tbody>\n\t\t<tr *ngFor=\"let stat of scpcservice.statsAsArray()\">\n\t\t<td>{{stat[0]}}</td>\n\t\t<td>{{stat[1]}}</td>\n\t\t</tr>\n\t\t</tbody>\n\t</table>\n</div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.component.ts ***!
  \********************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_scpc_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/scpc.service */ "./src/app/services/scpc.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(scpcservice) {
        this.scpcservice = scpcservice;
    }
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/pages/dashboard/dashboard.component.html"),
        }),
        __metadata("design:paramtypes", [_services_scpc_service__WEBPACK_IMPORTED_MODULE_1__["SCPCService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.module.ts ***!
  \*****************************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/pages/dashboard/dashboard.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__["ThemeModule"],
            ],
            declarations: [
                _dashboard_component__WEBPACK_IMPORTED_MODULE_2__["DashboardComponent"],
            ],
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/pages/miscellaneous/miscellaneous-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/miscellaneous/miscellaneous-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: MiscellaneousRoutingModule, routedComponents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MiscellaneousRoutingModule", function() { return MiscellaneousRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routedComponents", function() { return routedComponents; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _miscellaneous_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./miscellaneous.component */ "./src/app/pages/miscellaneous/miscellaneous.component.ts");
/* harmony import */ var _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./not-found/not-found.component */ "./src/app/pages/miscellaneous/not-found/not-found.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [{
        path: '',
        component: _miscellaneous_component__WEBPACK_IMPORTED_MODULE_2__["MiscellaneousComponent"],
        children: [{
                path: '404',
                component: _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_3__["NotFoundComponent"],
            }],
    }];
var MiscellaneousRoutingModule = /** @class */ (function () {
    function MiscellaneousRoutingModule() {
    }
    MiscellaneousRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], MiscellaneousRoutingModule);
    return MiscellaneousRoutingModule;
}());

var routedComponents = [
    _miscellaneous_component__WEBPACK_IMPORTED_MODULE_2__["MiscellaneousComponent"],
    _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_3__["NotFoundComponent"],
];


/***/ }),

/***/ "./src/app/pages/miscellaneous/miscellaneous.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/pages/miscellaneous/miscellaneous.component.ts ***!
  \****************************************************************/
/*! exports provided: MiscellaneousComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MiscellaneousComponent", function() { return MiscellaneousComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MiscellaneousComponent = /** @class */ (function () {
    function MiscellaneousComponent() {
    }
    MiscellaneousComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-miscellaneous',
            template: "\n    <router-outlet></router-outlet>\n  ",
        })
    ], MiscellaneousComponent);
    return MiscellaneousComponent;
}());



/***/ }),

/***/ "./src/app/pages/miscellaneous/miscellaneous.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/miscellaneous/miscellaneous.module.ts ***!
  \*************************************************************/
/*! exports provided: MiscellaneousModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MiscellaneousModule", function() { return MiscellaneousModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _miscellaneous_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./miscellaneous-routing.module */ "./src/app/pages/miscellaneous/miscellaneous-routing.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MiscellaneousModule = /** @class */ (function () {
    function MiscellaneousModule() {
    }
    MiscellaneousModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__["ThemeModule"],
                _miscellaneous_routing_module__WEBPACK_IMPORTED_MODULE_2__["MiscellaneousRoutingModule"],
            ],
            declarations: _miscellaneous_routing_module__WEBPACK_IMPORTED_MODULE_2__["routedComponents"].slice(),
        })
    ], MiscellaneousModule);
    return MiscellaneousModule;
}());



/***/ }),

/***/ "./src/app/pages/miscellaneous/not-found/not-found.component.html":
/*!************************************************************************!*\
  !*** ./src/app/pages/miscellaneous/not-found/not-found.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n\t<div class=\"col-md-12\">\n\t\t<nb-card> <nb-card-body>\n\t\t<div class=\"flex-centered col-xl-4 col-lg-6 col-md-8 col-sm-12\">\n\t\t\t<h2 class=\"title\">404 Page Not Found</h2>\n\t\t\t<small class=\"sub-title\">The page you were looking for\n\t\t\t\tdoesn't exist</small>\n\t\t\t<button (click)=\"goToHome()\" type=\"button\"\n\t\t\t\tclass=\"btn btn-block btn-hero-primary\">Take me home</button>\n\t\t</div>\n\t\t</nb-card-body> </nb-card>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/pages/miscellaneous/not-found/not-found.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/pages/miscellaneous/not-found/not-found.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-centered {\n  margin: auto; }\n\nnb-card-body {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n\n.title {\n  text-align: center; }\n\n.sub-title {\n  text-align: center;\n  display: block;\n  margin-bottom: 3rem; }\n\n.btn {\n  margin-bottom: 2rem; }\n"

/***/ }),

/***/ "./src/app/pages/miscellaneous/not-found/not-found.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/miscellaneous/not-found/not-found.component.ts ***!
  \**********************************************************************/
/*! exports provided: NotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundComponent", function() { return NotFoundComponent; });
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NotFoundComponent = /** @class */ (function () {
    function NotFoundComponent(menuService) {
        this.menuService = menuService;
    }
    NotFoundComponent.prototype.goToHome = function () {
        this.menuService.navigateHome();
    };
    NotFoundComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-not-found',
            styles: [__webpack_require__(/*! ./not-found.component.scss */ "./src/app/pages/miscellaneous/not-found/not-found.component.scss")],
            template: __webpack_require__(/*! ./not-found.component.html */ "./src/app/pages/miscellaneous/not-found/not-found.component.html"),
        }),
        __metadata("design:paramtypes", [_nebular_theme__WEBPACK_IMPORTED_MODULE_0__["NbMenuService"]])
    ], NotFoundComponent);
    return NotFoundComponent;
}());



/***/ }),

/***/ "./src/app/pages/network/network.component.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/network/network.component.ts ***!
  \****************************************************/
/*! exports provided: NetworkComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NetworkComponent", function() { return NetworkComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/index.js");
/* harmony import */ var rxjs_operators_takeWhile__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators/takeWhile */ "./node_modules/rxjs-compat/_esm5/operators/takeWhile.js");
/* harmony import */ var _services_input_theme_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/input-theme.service */ "./src/app/services/input-theme.service.ts");
/* harmony import */ var _services_scpc_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/scpc.service */ "./src/app/services/scpc.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NetworkComponent = /** @class */ (function () {
    function NetworkComponent(themeService, inputservice, scpcservice) {
        var _this = this;
        this.themeService = themeService;
        this.inputservice = inputservice;
        this.scpcservice = scpcservice;
        this.alive = true;
        this.themeService.getJsTheme()
            .pipe(Object(rxjs_operators_takeWhile__WEBPACK_IMPORTED_MODULE_2__["takeWhile"])(function () { return _this.alive; }));
    }
    NetworkComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    NetworkComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-network',
            template: __webpack_require__(/*! ./network.html */ "./src/app/pages/network/network.html"),
        }),
        __metadata("design:paramtypes", [_nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbThemeService"],
            _services_input_theme_service__WEBPACK_IMPORTED_MODULE_3__["InputThemeService"],
            _services_scpc_service__WEBPACK_IMPORTED_MODULE_4__["SCPCService"]])
    ], NetworkComponent);
    return NetworkComponent;
}());



/***/ }),

/***/ "./src/app/pages/network/network.html":
/*!********************************************!*\
  !*** ./src/app/pages/network/network.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n\t<div class=\"col-md-6 col-xxl-6 col-sm-12\">\n\t\t<nb-card> <nb-card-header>Network&nbsp;settings</nb-card-header>\n\t\t<nb-card-body>\n\t\t<div class=\"form-group row\">\n\t\t\t<label for=\"inputTermIP\" class=\"col-sm-3 col-form-label\">Terminal&nbsp;IP</label>\n\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"TermIP\"\n\t\t\t\t[disabled]=\"!inputservice.editable\"\n\t\t\t\t[(ngModel)]=\"scpcservice.stats.TermIP\"\n\t\t\t\t[class.form-control-warning]=\"inputservice.isChanged('TermIP')\"\n\t\t\t\t(change)=\"inputservice.changeInput($event)\"\n\t\t\t\t(keydown)=\"inputservice.changeInput($event)\"\n\t\t\t\tplaceholder=\"192.168.1.1\">\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"form-group row\">\n\t\t\t<label for=\"inputRemoteTermIP\" class=\"col-sm-3 col-form-label\">Remote&nbsp;Terminal&nbsp;IP</label>\n\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"RemoteTermIP\"\n\t\t\t\t[disabled]=\"!inputservice.editable\"\n\t\t\t\t[(ngModel)]=\"scpcservice.stats.RemoteTermIP\"\n\t\t\t\t[class.form-control-warning]=\"inputservice.isChanged('RemoteTermIP')\"\n\t\t\t\t(change)=\"inputservice.changeInput($event)\"\n\t\t\t\t(keydown)=\"inputservice.changeInput($event)\"\n\t\t\t\tplaceholder=\"192.168.1.255\">\n\t\t\t</div>\n\t\t</div>\n\t\t</nb-card-body> </nb-card>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/pages/network/network.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/network/network.module.ts ***!
  \*************************************************/
/*! exports provided: NetworkModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NetworkModule", function() { return NetworkModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _network_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./network.component */ "./src/app/pages/network/network.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NetworkModule = /** @class */ (function () {
    function NetworkModule() {
    }
    NetworkModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__["ThemeModule"],
            ],
            declarations: [
                _network_component__WEBPACK_IMPORTED_MODULE_2__["NetworkComponent"],
            ],
        })
    ], NetworkModule);
    return NetworkModule;
}());



/***/ }),

/***/ "./src/app/pages/pages-menu.ts":
/*!*************************************!*\
  !*** ./src/app/pages/pages-menu.ts ***!
  \*************************************/
/*! exports provided: MENU_ITEMS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MENU_ITEMS", function() { return MENU_ITEMS; });
var MENU_ITEMS = [
    {
        title: 'Dashboard',
        icon: 'nb-home',
        link: '/pages/dashboard',
        home: true,
    },
    {
        title: 'Status page',
        icon: 'ion ion-stats-bars',
        link: '/pages/status',
    },
    {
        title: 'Network',
        icon: 'ion ion-network',
        link: '/pages/network',
    },
    {
        title: 'RRM',
        icon: 'ion ion-radio-waves',
        link: '/pages/rrm',
    },
    {
        title: 'Update',
        icon: 'nb-loop',
        link: '/pages/version',
        children: [
            {
                title: 'Modem firmware',
                link: '/pages/modem-firmware',
            },
            {
                title: 'SCPC firmware',
                link: '/pages/network-processor-firmware',
            },
            {
                title: 'WEB interface',
                link: '/pages/update/web',
            },
        ],
    },
    {
        title: 'API',
        icon: 'ion ion-ios-cog',
        link: '/pages/api',
    },
];


/***/ }),

/***/ "./src/app/pages/pages-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/pages-routing.module.ts ***!
  \***********************************************/
/*! exports provided: PagesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesRoutingModule", function() { return PagesRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _pages_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pages.component */ "./src/app/pages/pages.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/pages/dashboard/dashboard.component.ts");
/* harmony import */ var _update_modem_firmware_modem_firmware_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./update/modem-firmware/modem-firmware.component */ "./src/app/pages/update/modem-firmware/modem-firmware.component.ts");
/* harmony import */ var _update_web_web_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./update/web/web.component */ "./src/app/pages/update/web/web.component.ts");
/* harmony import */ var _update_network_processor_firmware_network_processor_firmware_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./update/network-processor-firmware/network-processor-firmware.component */ "./src/app/pages/update/network-processor-firmware/network-processor-firmware.component.ts");
/* harmony import */ var _network_network_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./network/network.component */ "./src/app/pages/network/network.component.ts");
/* harmony import */ var _status_status_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./status/status.component */ "./src/app/pages/status/status.component.ts");
/* harmony import */ var _api_api_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./api/api.component */ "./src/app/pages/api/api.component.ts");
/* harmony import */ var _rrm_rrm_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./rrm/rrm.component */ "./src/app/pages/rrm/rrm.component.ts");
/* harmony import */ var _services_auth_guard_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../services/auth-guard.service */ "./src/app/services/auth-guard.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var routes = [{
        path: '',
        component: _pages_component__WEBPACK_IMPORTED_MODULE_2__["PagesComponent"],
        children: [
            {
                path: 'dashboard',
                component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"],
            },
            {
                path: 'modem-firmware',
                component: _update_modem_firmware_modem_firmware_component__WEBPACK_IMPORTED_MODULE_4__["ModemFirmwareComponent"],
                canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_11__["AuthGuard"]],
            },
            {
                path: 'network-processor-firmware',
                component: _update_network_processor_firmware_network_processor_firmware_component__WEBPACK_IMPORTED_MODULE_6__["NetworkProcessorFirmwareComponent"],
                canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_11__["AuthGuard"]],
            },
            {
                path: 'update/web',
                component: _update_web_web_component__WEBPACK_IMPORTED_MODULE_5__["WebComponent"],
                canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_11__["AuthGuard"]],
            },
            {
                path: 'network',
                component: _network_network_component__WEBPACK_IMPORTED_MODULE_7__["NetworkComponent"],
            },
            {
                path: 'status',
                component: _status_status_component__WEBPACK_IMPORTED_MODULE_8__["StatusComponent"],
            },
            {
                path: 'rrm',
                component: _rrm_rrm_component__WEBPACK_IMPORTED_MODULE_10__["RRMComponent"],
            },
            {
                path: 'api',
                component: _api_api_component__WEBPACK_IMPORTED_MODULE_9__["ApiComponent"],
            },
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full',
            },
        ],
    }];
var PagesRoutingModule = /** @class */ (function () {
    function PagesRoutingModule() {
    }
    PagesRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]],
        })
    ], PagesRoutingModule);
    return PagesRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/pages.component.ts":
/*!******************************************!*\
  !*** ./src/app/pages/pages.component.ts ***!
  \******************************************/
/*! exports provided: PagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesComponent", function() { return PagesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_guard_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/auth-guard.service */ "./src/app/services/auth-guard.service.ts");
/* harmony import */ var _pages_menu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pages-menu */ "./src/app/pages/pages-menu.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PagesComponent = /** @class */ (function () {
    function PagesComponent(authguard) {
        var _this = this;
        this.authguard = authguard;
        this.menu = _pages_menu__WEBPACK_IMPORTED_MODULE_2__["MENU_ITEMS"];
        this.authguard.authService.onAuthenticationChange().subscribe(function (status) { _this.menu.find(function (o) { return o.link === '/pages/version'; }).hidden = !status; });
    }
    PagesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-pages',
            template: "\n    <ngx-sample-layout>\n      <nb-menu [items]=\"menu\"></nb-menu>\n      <router-outlet></router-outlet>\n    </ngx-sample-layout>\n  ",
        }),
        __metadata("design:paramtypes", [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_1__["AuthGuard"]])
    ], PagesComponent);
    return PagesComponent;
}());



/***/ }),

/***/ "./src/app/pages/pages.module.ts":
/*!***************************************!*\
  !*** ./src/app/pages/pages.module.ts ***!
  \***************************************/
/*! exports provided: PagesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesModule", function() { return PagesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _pages_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pages.component */ "./src/app/pages/pages.component.ts");
/* harmony import */ var _dashboard_dashboard_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard/dashboard.module */ "./src/app/pages/dashboard/dashboard.module.ts");
/* harmony import */ var _update_modem_firmware_modem_firmware_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./update/modem-firmware/modem-firmware.module */ "./src/app/pages/update/modem-firmware/modem-firmware.module.ts");
/* harmony import */ var _update_network_processor_firmware_network_processor_firmware_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./update/network-processor-firmware/network-processor-firmware.module */ "./src/app/pages/update/network-processor-firmware/network-processor-firmware.module.ts");
/* harmony import */ var _rrm_rrm_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./rrm/rrm.module */ "./src/app/pages/rrm/rrm.module.ts");
/* harmony import */ var _network_network_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./network/network.module */ "./src/app/pages/network/network.module.ts");
/* harmony import */ var _update_web_web_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./update/web/web.module */ "./src/app/pages/update/web/web.module.ts");
/* harmony import */ var _status_status_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./status/status.module */ "./src/app/pages/status/status.module.ts");
/* harmony import */ var _api_api_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./api/api.module */ "./src/app/pages/api/api.module.ts");
/* harmony import */ var _pages_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pages-routing.module */ "./src/app/pages/pages-routing.module.ts");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _miscellaneous_miscellaneous_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./miscellaneous/miscellaneous.module */ "./src/app/pages/miscellaneous/miscellaneous.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var PAGES_COMPONENTS = [
    _pages_component__WEBPACK_IMPORTED_MODULE_1__["PagesComponent"],
];
var PagesModule = /** @class */ (function () {
    function PagesModule() {
    }
    PagesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _pages_routing_module__WEBPACK_IMPORTED_MODULE_10__["PagesRoutingModule"],
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_11__["ThemeModule"],
                _dashboard_dashboard_module__WEBPACK_IMPORTED_MODULE_2__["DashboardModule"],
                _update_modem_firmware_modem_firmware_module__WEBPACK_IMPORTED_MODULE_3__["ModemFirmwareModule"],
                _rrm_rrm_module__WEBPACK_IMPORTED_MODULE_5__["RRMModule"],
                _network_network_module__WEBPACK_IMPORTED_MODULE_6__["NetworkModule"],
                _update_web_web_module__WEBPACK_IMPORTED_MODULE_7__["WebModule"],
                _api_api_module__WEBPACK_IMPORTED_MODULE_9__["ApiModule"],
                _status_status_module__WEBPACK_IMPORTED_MODULE_8__["StatusModule"],
                _update_network_processor_firmware_network_processor_firmware_module__WEBPACK_IMPORTED_MODULE_4__["NetworkProcessorFirmwareModule"],
                _miscellaneous_miscellaneous_module__WEBPACK_IMPORTED_MODULE_12__["MiscellaneousModule"],
            ],
            declarations: PAGES_COMPONENTS.slice(),
        })
    ], PagesModule);
    return PagesModule;
}());



/***/ }),

/***/ "./src/app/pages/rrm/rrm.component.html":
/*!**********************************************!*\
  !*** ./src/app/pages/rrm/rrm.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n\t<div class=\"col-md-6 col-xxl-6 col-sm-12\">\n\t\t<nb-card> <nb-card-header>Radio&nbsp;Resource&nbsp;Manager&nbsp;settings</nb-card-header>\n\t\t<nb-card-body>\n\t\t<div class=\"form-group row\">\n\t\t\t<label for=\"inputRRMIP\" class=\"col-sm-3 col-form-label\">RRM&nbsp;IP</label>\n\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"RRMIP\"\n\t\t\t\t\t[disabled]=\"!inputservice.editable\"\n\t\t\t\t\t[class.form-control-warning]=\"inputservice.isChanged('RRMIP')\"\n\t\t\t\t\t[(ngModel)]=\"scpcservice.stats.RRMIP\"\n\t\t\t\t\t(change)=\"inputservice.changeInput($event)\"\n\t\t\t\t\t(keydown)=\"inputservice.changeInput($event)\"\n\t\t\t\t\tplaceholder=\"192.168.1.1\">\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"form-group row\">\n\t\t\t<label for=\"inputRRMAuth\" class=\"col-sm-3 col-form-label\">RRM&nbsp;Auth</label>\n\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"RRMAuth\"\n\t\t\t\t\t(change)=\"inputservice.changeInput($event)\" placeholder=\"secret\">\n\t\t\t</div>\n\t\t</div>\n\t\t</nb-card-body> </nb-card>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/pages/rrm/rrm.component.ts":
/*!********************************************!*\
  !*** ./src/app/pages/rrm/rrm.component.ts ***!
  \********************************************/
/*! exports provided: RRMComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RRMComponent", function() { return RRMComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/index.js");
/* harmony import */ var rxjs_operators_takeWhile__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators/takeWhile */ "./node_modules/rxjs-compat/_esm5/operators/takeWhile.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RRMComponent = /** @class */ (function () {
    function RRMComponent(themeService, inputservice, scpcservice) {
        var _this = this;
        this.themeService = themeService;
        this.inputservice = inputservice;
        this.scpcservice = scpcservice;
        this.alive = true;
        this.themeService.getJsTheme()
            .pipe(Object(rxjs_operators_takeWhile__WEBPACK_IMPORTED_MODULE_2__["takeWhile"])(function () { return _this.alive; }));
    }
    RRMComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    RRMComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-rrm',
            template: __webpack_require__(/*! ./rrm.component.html */ "./src/app/pages/rrm/rrm.component.html"),
        }),
        __metadata("design:paramtypes", [_nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbThemeService"],
            _services__WEBPACK_IMPORTED_MODULE_3__["InputThemeService"],
            _services__WEBPACK_IMPORTED_MODULE_3__["SCPCService"]])
    ], RRMComponent);
    return RRMComponent;
}());



/***/ }),

/***/ "./src/app/pages/rrm/rrm.module.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/rrm/rrm.module.ts ***!
  \*****************************************/
/*! exports provided: RRMModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RRMModule", function() { return RRMModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _rrm_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./rrm.component */ "./src/app/pages/rrm/rrm.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RRMModule = /** @class */ (function () {
    function RRMModule() {
    }
    RRMModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__["ThemeModule"],
            ],
            declarations: [
                _rrm_component__WEBPACK_IMPORTED_MODULE_2__["RRMComponent"],
            ],
        })
    ], RRMModule);
    return RRMModule;
}());



/***/ }),

/***/ "./src/app/pages/status/status.component.html":
/*!****************************************************!*\
  !*** ./src/app/pages/status/status.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n\t<div class=\"col-md-6 col-xxl-6\">\n\t\t<nb-card> <nb-card-header>{{ 'Rx status' | translate }}</nb-card-header> <nb-card-body>\n\t\t\t<div class=\"form-group row\">\n\t\t\t\t<label for=\"inputRxSR\" class=\"col-sm-3 col-form-label\">SR&nbsp;RX&nbsp;(MSps)</label>\n\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t<input type=\"number\" class=\"form-control input-warning\" id=\"RxSR\" [disabled]=\"!inputservice.editable\"\n\t\t\t\t\t\t[(ngModel)]=\"scpcservice.stats.RxSR\"\n\t\t\t\t\t\t[class.form-control-warning]=\"inputservice.isChanged('RxSR')\"\n\t\t\t\t\t\t(change)=\"inputservice.changeInput($event)\"\n\t\t\t\t\t\t(keydown)=\"inputservice.changeInput($event)\"\n\t\t\t\t\t\tplaceholder=\"0.3-125\">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"form-group row\">\n\t\t\t\t<label for=\"inputRxFreq\" class=\"col-sm-3 col-form-label\">Freq&nbsp;RX&nbsp;(Mhz)</label>\n\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"RxFreq\" [disabled]=\"!inputservice.editable\"\n\t\t\t\t\t\t[(ngModel)]=\"scpcservice.stats.RxFreq\"\n\t\t\t\t\t\t[class.form-control-warning]=\"inputservice.isChanged('RxFreq')\"\n\t\t\t\t\t\t(change)=\"inputservice.changeInput($event)\"\n\t\t\t\t\t\t(keydown)=\"inputservice.changeInput($event)\"\n\t\t\t\t\t\tplaceholder=\"950 - 1750\">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"form-group row\">\n\t\t\t\t<label for=\"switcherRxLNBPower\" class=\"col-sm-3 col-form-label\">LNB&nbsp;Status&nbsp;RX</label>\n\t\t\t\t<div class=\"col-sm-3 text-left\">\n\t\t\t\t\t<ngx-switcher [disabled]=\"!inputservice.editable\" \n\t\t\t\t\t\t[id]=\"'RxLNBPower'\"\n\t\t\t\t\t\t[firstValue]=\"0\"\n\t\t\t\t\t\t[secondValue]=\"1\"\n\t\t\t\t\t\t[firstValueLabel]=\"'Off'\"\n\t\t\t\t\t\t[secondValueLabel]=\"'On'\"\n\t\t\t\t\t\t[(ngModel)]=\"scpcservice.stats.RxLNBPower\"\n\t\t\t\t\t\t(change)=\"inputservice.changeInput($event)\"\n\t\t\t\t\t\t[changed]=\"inputservice.isChanged('RxLNBPower')\"\n\t\t\t\t\t\t(click)=\"inputservice.changeInput($event)\">\n\t\t\t\t\t\t</ngx-switcher>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<!--\n\t\t\t<div class=\"form-group row\">\n\t\t\t\t<label for=\"inputplsrcv\" class=\"col-sm-3 col-form-label\">PLS&nbsp;RCV</label>\n\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t<select class=\"form-control\" id=\"plsrcv\">\n\t\t\t\t\t\t<option>1</option>\n\t\t\t\t\t\t<option>2</option>\n\t\t\t\t\t\t<option>3</option>\n\t\t\t\t\t\t<option>4</option>\n\t\t\t\t\t\t<option>5</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"form-group row\">\n\t\t\t\t<div class=\"offset-sm-3 col-sm-9\">\n\t\t\t\t\t<nb-checkbox id=\"txlnblo\">LNB&nbsp;LO</nb-checkbox>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"form-group row\">\n\t\t\t\t<div class=\"offset-sm-3 col-sm-9\">\n\t\t\t\t\t<nb-checkbox id=\"tx10mhz\">10Mhz</nb-checkbox>\n\t\t\t\t</div>\n\t\t\t</div> -->\n\t\t</nb-card-body> </nb-card>\n\t</div>\n\t<div class=\"col-md-6 col-xxl-6\">\n\t\t<nb-card> <nb-card-header> Tx&nbsp;Status</nb-card-header> <nb-card-body>\n\t\t<!--\n\t\t<ul>\n\t\t\t<li>BUC LO</li>\n\t\t\t<li>10Mhz on/off</li>\n\t\t\t<li>CW on/off</li>\n\t\t\t<li>Freq TX (Mhz)</li>\n\t\t\t<li>SR TX (Mhz)</li>\n\t\t\t<li>PLS TX</li>\n\t\t\t<li>PWR TX\n\t\t\t<li>CNTRL TX (ON/OFF)</li>\n\t\t\t<li>ACM on/off\n\t\t\t\t<ul>\n\t\t\t\t\t<li>ACM PLS list/thresholds (for future)</li>\n\t\t\t\t\t<li>ACM margin</li>\n\t\t\t\t</ul>\n\t\t\t</li>\n\t\t\t<li>ULPC on/off\n\t\t\t\t<ul>\n\t\t\t\t\t<li>Short on/off</li>\n\t\t\t\t\t<li>Long on/off</li>\n\t\t\t\t</ul>\n\t\t\t</li>\n\t\t\t<li>P1dB search start/stop</li>\n\t\t</ul>\n\t\t-->\n\t\t\t<div class=\"form-group row\">\n\t\t\t\t<label for=\"inputTxSR\" class=\"col-sm-3 col-form-label\">SR&nbsp;TX&nbsp;(Msps)</label>\n\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t<input type=\"number\" class=\"form-control input-warning\" id=\"TxSR\" [disabled]=\"!inputservice.editable\"\n\t\t\t\t\t\t[(ngModel)]=\"scpcservice.stats.TxSR\"\n\t\t\t\t\t\t[class.form-control-warning]=\"inputservice.isChanged('TxSR')\"\n\t\t\t\t\t\t(change)=\"inputservice.changeInput($event)\"\n\t\t\t\t\t\t(keydown)=\"inputservice.changeInput($event)\"\n\t\t\t\t\t\tplaceholder=\"0.3-125\">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"form-group row\">\n\t\t\t\t<label for=\"inputTxFreq\" class=\"col-sm-3 col-form-label\">Freq&nbsp;TX&nbsp;(Mhz)</label>\n\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"TxFreq\" [disabled]=\"!inputservice.editable\" \n\t\t\t\t\t\t[(ngModel)]=\"scpcservice.stats.TxFreq\"\n\t\t\t\t\t\t[class.form-control-warning]=\"inputservice.isChanged('TxFreq')\"\n\t\t\t\t\t\t(change)=\"inputservice.changeInput($event)\"\n\t\t\t\t\t\tplaceholder=\"950 - 1750\">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"form-group row\">\n\t\t\t\t<label for=\"inputTxPWR\" class=\"col-sm-3 col-form-label\">TX&nbsp; power (dbm)</label>\n\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"TxPWR\" [disabled]=\"!inputservice.editable\"\n\t\t\t\t\t\t[(ngModel)]=\"scpcservice.stats.TxPWR\"\n\t\t\t\t\t\t[class.form-control-warning]=\"inputservice.isChanged('TxPWR')\"\n\t\t\t\t\t\t(keydown)=\"inputservice.changeInput($event)\"\n\t\t\t\t\t\t[ngModelOptions]=\"{standalone: true}\"\n\t\t\t\t\t\t(change)=\"inputservice.changeInput($event)\">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"form-group row\">\n\t\t\t\t<label for=\"selectTxPLS\" class=\"col-sm-3 col-form-label\">TX&nbsp;PLS</label>\n\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t<select class=\"form-control\" id=\"TxPLS\" [(ngModel)]=\"scpcservice.stats.TxPLS\" [disabled]=\"!inputservice.editable\"\n\t\t\t\t\t\t(click)=\"inputservice.changeInput($event)\"\n\t\t\t\t\t\t[class.form-control-warning]=\"inputservice.isChanged('TxPLS')\"\n\t\t\t\t\t\t(change)=\"inputservice.changeInput($event)\">\n\t\t\t\t\t\t<option *ngFor=\"let row of scpcservice.snr\" value=\"{{row[0]}}\">{{row[1]}}</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</nb-card-body> </nb-card>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/pages/status/status.component.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/status/status.component.ts ***!
  \**************************************************/
/*! exports provided: StatusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatusComponent", function() { return StatusComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_input_theme_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/input-theme.service */ "./src/app/services/input-theme.service.ts");
/* harmony import */ var _services_scpc_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/scpc.service */ "./src/app/services/scpc.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StatusComponent = /** @class */ (function () {
    function StatusComponent(inputservice, scpcservice) {
        this.inputservice = inputservice;
        this.scpcservice = scpcservice;
    }
    StatusComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-status',
            template: __webpack_require__(/*! ./status.component.html */ "./src/app/pages/status/status.component.html"),
            styles: [__webpack_require__(/*! ./status.scss */ "./src/app/pages/status/status.scss")],
        }),
        __metadata("design:paramtypes", [_services_input_theme_service__WEBPACK_IMPORTED_MODULE_1__["InputThemeService"],
            _services_scpc_service__WEBPACK_IMPORTED_MODULE_2__["SCPCService"]])
    ], StatusComponent);
    return StatusComponent;
}());



/***/ }),

/***/ "./src/app/pages/status/status.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/status/status.module.ts ***!
  \***********************************************/
/*! exports provided: StatusModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatusModule", function() { return StatusModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _status_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./status.component */ "./src/app/pages/status/status.component.ts");
/* harmony import */ var _services_input_theme_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/input-theme.service */ "./src/app/services/input-theme.service.ts");
/* harmony import */ var _services_scpc_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/scpc.service */ "./src/app/services/scpc.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var StatusModule = /** @class */ (function () {
    function StatusModule() {
    }
    StatusModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_3__["ThemeModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClientModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateModule"],
            ],
            declarations: [
                _status_component__WEBPACK_IMPORTED_MODULE_4__["StatusComponent"],
            ],
            providers: [_services_input_theme_service__WEBPACK_IMPORTED_MODULE_5__["InputThemeService"], _services_scpc_service__WEBPACK_IMPORTED_MODULE_6__["SCPCService"]],
            exports: [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateModule"]],
        })
    ], StatusModule);
    return StatusModule;
}());



/***/ }),

/***/ "./src/app/pages/status/status.scss":
/*!******************************************!*\
  !*** ./src/app/pages/status/status.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "input[type=number]::-webkit-inner-spin-button,\ninput[type=number]::-webkit-outer-spin-button {\n  -webkit-appearance: none;\n  margin: 0; }\n"

/***/ }),

/***/ "./src/app/pages/update/modem-firmware/modem-firmware.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/update/modem-firmware/modem-firmware.component.ts ***!
  \*************************************************************************/
/*! exports provided: ModemFirmwareComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModemFirmwareComponent", function() { return ModemFirmwareComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/index.js");
/* harmony import */ var rxjs_operators_takeWhile__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators/takeWhile */ "./node_modules/rxjs-compat/_esm5/operators/takeWhile.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ModemFirmwareComponent = /** @class */ (function () {
    function ModemFirmwareComponent(themeService) {
        var _this = this;
        this.themeService = themeService;
        this.alive = true;
        this.themeService.getJsTheme()
            .pipe(Object(rxjs_operators_takeWhile__WEBPACK_IMPORTED_MODULE_2__["takeWhile"])(function () { return _this.alive; }));
    }
    ModemFirmwareComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-modem-firmware',
            template: __webpack_require__(/*! ./modem-firmware.html */ "./src/app/pages/update/modem-firmware/modem-firmware.html"),
        }),
        __metadata("design:paramtypes", [_nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbThemeService"]])
    ], ModemFirmwareComponent);
    return ModemFirmwareComponent;
}());



/***/ }),

/***/ "./src/app/pages/update/modem-firmware/modem-firmware.html":
/*!*****************************************************************!*\
  !*** ./src/app/pages/update/modem-firmware/modem-firmware.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">Terminal software versions and update tools will\n\tbe here</div>"

/***/ }),

/***/ "./src/app/pages/update/modem-firmware/modem-firmware.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/update/modem-firmware/modem-firmware.module.ts ***!
  \**********************************************************************/
/*! exports provided: ModemFirmwareModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModemFirmwareModule", function() { return ModemFirmwareModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _modem_firmware_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modem-firmware.component */ "./src/app/pages/update/modem-firmware/modem-firmware.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ModemFirmwareModule = /** @class */ (function () {
    function ModemFirmwareModule() {
    }
    ModemFirmwareModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__["ThemeModule"],
            ],
            declarations: [
                _modem_firmware_component__WEBPACK_IMPORTED_MODULE_2__["ModemFirmwareComponent"],
            ],
        })
    ], ModemFirmwareModule);
    return ModemFirmwareModule;
}());



/***/ }),

/***/ "./src/app/pages/update/network-processor-firmware/network-processor-firmware.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/pages/update/network-processor-firmware/network-processor-firmware.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: NetworkProcessorFirmwareComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NetworkProcessorFirmwareComponent", function() { return NetworkProcessorFirmwareComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/index.js");
/* harmony import */ var rxjs_operators_takeWhile__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators/takeWhile */ "./node_modules/rxjs-compat/_esm5/operators/takeWhile.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NetworkProcessorFirmwareComponent = /** @class */ (function () {
    function NetworkProcessorFirmwareComponent(themeService) {
        var _this = this;
        this.themeService = themeService;
        this.alive = true;
        this.themeService.getJsTheme()
            .pipe(Object(rxjs_operators_takeWhile__WEBPACK_IMPORTED_MODULE_2__["takeWhile"])(function () { return _this.alive; }));
    }
    NetworkProcessorFirmwareComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-network-processor-firmware',
            template: __webpack_require__(/*! ./network-processor-firmware.html */ "./src/app/pages/update/network-processor-firmware/network-processor-firmware.html"),
        }),
        __metadata("design:paramtypes", [_nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbThemeService"]])
    ], NetworkProcessorFirmwareComponent);
    return NetworkProcessorFirmwareComponent;
}());



/***/ }),

/***/ "./src/app/pages/update/network-processor-firmware/network-processor-firmware.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/update/network-processor-firmware/network-processor-firmware.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">Network processor software versions and update\n\ttools will be here</div>"

/***/ }),

/***/ "./src/app/pages/update/network-processor-firmware/network-processor-firmware.module.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/pages/update/network-processor-firmware/network-processor-firmware.module.ts ***!
  \**********************************************************************************************/
/*! exports provided: NetworkProcessorFirmwareModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NetworkProcessorFirmwareModule", function() { return NetworkProcessorFirmwareModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _network_processor_firmware_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./network-processor-firmware.component */ "./src/app/pages/update/network-processor-firmware/network-processor-firmware.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NetworkProcessorFirmwareModule = /** @class */ (function () {
    function NetworkProcessorFirmwareModule() {
    }
    NetworkProcessorFirmwareModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__["ThemeModule"],
            ],
            declarations: [
                _network_processor_firmware_component__WEBPACK_IMPORTED_MODULE_2__["NetworkProcessorFirmwareComponent"],
            ],
        })
    ], NetworkProcessorFirmwareModule);
    return NetworkProcessorFirmwareModule;
}());



/***/ }),

/***/ "./src/app/pages/update/web/web.component.html":
/*!*****************************************************!*\
  !*** ./src/app/pages/update/web/web.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\nInfo - update\n\n</div>"

/***/ }),

/***/ "./src/app/pages/update/web/web.component.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/update/web/web.component.ts ***!
  \***************************************************/
/*! exports provided: WebComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebComponent", function() { return WebComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/index.js");
/* harmony import */ var rxjs_operators_takeWhile__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators/takeWhile */ "./node_modules/rxjs-compat/_esm5/operators/takeWhile.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WebComponent = /** @class */ (function () {
    function WebComponent(themeService) {
        var _this = this;
        this.themeService = themeService;
        this.alive = true;
        this.themeService.getJsTheme()
            .pipe(Object(rxjs_operators_takeWhile__WEBPACK_IMPORTED_MODULE_2__["takeWhile"])(function () { return _this.alive; }));
    }
    WebComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-modem-firmware',
            template: __webpack_require__(/*! ./web.component.html */ "./src/app/pages/update/web/web.component.html"),
        }),
        __metadata("design:paramtypes", [_nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbThemeService"]])
    ], WebComponent);
    return WebComponent;
}());



/***/ }),

/***/ "./src/app/pages/update/web/web.module.ts":
/*!************************************************!*\
  !*** ./src/app/pages/update/web/web.module.ts ***!
  \************************************************/
/*! exports provided: WebModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebModule", function() { return WebModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _web_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./web.component */ "./src/app/pages/update/web/web.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var WebModule = /** @class */ (function () {
    function WebModule() {
    }
    WebModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__["ThemeModule"],
            ],
            declarations: [
                _web_component__WEBPACK_IMPORTED_MODULE_2__["WebComponent"],
            ],
        })
    ], WebModule);
    return WebModule;
}());



/***/ })

}]);
//# sourceMappingURL=app-pages-pages-module.js.map