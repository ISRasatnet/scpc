import paramiko
import threading
import socket
from struct import*
import time
import Tkinter as tk

import math
import numpy as np
import sys
import random
import netifaces as ni
import psutil
import Queue
import copy
from flask import jsonify
# just for commit
#######################################################
#SERVER
#######################################################
#test
# import Tkinter as tk
Q_print = Queue.Queue(2)
class window(tk.Frame):
    def __init__(self, *args, **kwargs):
        tk.Frame.__init__(self, *args, **kwargs)

        self.text = tk.Text(self, height=20, width=40)
        self.vsb = tk.Scrollbar(self, orient="vertical", command=self.text.yview)
        self.text.configure(yscrollcommand=self.vsb.set)
        self.vsb.pack(side="right", fill="y")
        self.text.pack(side="left", fill="both", expand=True)


    def add_txt(self,txt):

        self.text.insert("end", str(txt) + "\n")
        self.text.see("end")
        self.update_idletasks()
        self.update()
        self.text_file(str(txt))
        #self.after(10,self.add_timestamp)

    def add_lable(self,lable):

        w = tk.Label(self, text= lable)
        w.pack()
    def text_file(seld,txt):
        text_file = open("Output.txt", "w")
        text_file.write(txt)
        #text_file.close()

switch =0b10000101
switch_list =[0b00000001,0b00000010,0b00000100,0b00001000,0b00010000,0b00100000,0b01000000,0b10000000]
switch_smart =[0,0,0,0,0,0,0,0]
current_Tx = -24  # lowest Tx Power Cfg option
target_snr = 12
Q_RTOScommand = Queue.Queue(1)
Q_tx_freq = Queue.Queue(1)
Q_packet = Queue.Queue(1)
Q_TX = Queue.Queue(1)
Q_delta_long = Queue.Queue(1)
Q_delta_long.empty()
Q_pls = Queue.Queue(1)
Q_pls.empty()
Q_delta  = Queue.Queue(1)
Q_delta.empty()
Q_reff  = Queue.Queue(1)
Q_reff_short = Queue.Queue(1)
modem_ip = '192.16.1.6'#'192.16.1.101'
modem_user = "root"
modem_password = "123456"
my_ip = '192.16.1.1'
my_static_ip = '192.16.1.5'
remote_ip = '192.16.1.8'
ipRRM = '192.168.70.188'
ID = 76
udp_port = 15617
# udp_port_server = 15617
# udp_port_client = 20375
delay = 1
tx_max = 0#-26
tx_max_real = 5
tx_min = -60
h = 1
merge = 1
pls_min = 132
pls_max = 178
current_pls = 156  ## thats the pls value we start with
snr = 0
counter = 0
snr_dict = {132:(-2.03,'QPSK 13/45'),134:(0.2,'QPSK 9/20'),136:(1.4,'QPSK 11/20'),138: (4.7, '8APSK 5/9-L'), 140: (5.1, '8APSK 26/45-L'), 142: (6, '16APSK 1/2-L'),
            144: (6.6, '16APSK 8/15-L'), 146: (6.8, '16APSK 5/9-L'), 148: (7.5, '16APSK 26/45'),
            150: (7.8, '16APSK 3/5'), 152: (8.1, '8APSK 26/45-L'), 154: (8.4, '16APSK 2/3-L'),
            156: (9.3, '16APSK 25/36'), 158: (9.7, '16APSK 13/18'), 160: (11.7, '16APSK 7/9'),
            162: (12, '16APSK 77/90'), 164: (12.2, '32APSK 11/15'), 166: (13.1, '32APSK 7/9'),
            168: (14, '64APSK 32/45-L'), 170: (14.8, '64APSK 11/15'), 172: (15.5, '64APSK 7/9'),
            174: (15.9, '64APSK 4/5'), 178: (16.9 , '128APSK 3/4'),
            180: (11.75,'32APSK 11/15'), 182: (13.05,'32APSK 7/9'), 184: (13.98,'64APSK 32/45-L'),
            186: (13.05,'64APSK 11/15'), 190: (15.47,'64APSK 7/9'), 194: (15.87,'64APSK 4/5'),
            198: (16.55,'64APSK 5/6'), 198: (16.55,'64APSK 5/6'), 204: (16.98,'256APSK 29/45-L'),
            206: (17.24,'256APSK 2/3-L'), 208: (18.1,'256APSK 31/45-L'), 210: (18.59,'256APSK 32/45'),
            212: (18.84,'256APSK 11/15-L'), 214: (19.57,'256APSK 3/4')}
#  cs = current_snr
def snr_target_modcod(cs):  # returns snr targer(QF) and modcod name
    global snr_dict
    pls_data = snr_dict[int(cs)]
    print pls_data
    return pls_data  # the data contains the snr target first and then the modcod

########################################################
def delay(t):
    time.sleep(t)
########################################################
def RTOSssh():
    RT_sock=create_socket(my_ip,4444)
    while True:
        if not Q_RTOScommand.empty():
            massege  = Q_RTOScommand.get()
            RT_sock.send(massege,(modem_ip,5555))


########################################################
def ssh_connection():

    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    rpi = {"username": modem_user, "password": modem_password, "hostname": modem_ip}
    try:
        ssh.connect(**rpi)
        print "connected"

    except:
        print "connection failed try again"
    return ssh

########################################################
def server():  ##because the other one is dumb
    global switch, switch_smart
    thread_number = 0b00000100
    while switch_smart[4] == 0:
        continue
    print "server started"
    global current_Tx
    global toggle
    global pls_data_list
    global snr
    global current_pls
    global my_ip, udp_port
    packet_counter =0
    packet_new = 0
    packet_old = 0
    stop = 0
    # s_server = create_socket('192.168.1.16', 15613)
    # s_server = create_socket('10.0.1.48', 15613)
    s_server = create_socket(my_ip, udp_port)

    # current_Tx = -40   ## local variable, not the same as current_tx in the rest of the script
    while True  :
        if switch_smart[4] == 1:
            s_server.settimeout(5)  ## waiting for udp packet to arrive for five seconds##### time out will make an error accord if it goes off
            try:
                packet_new = s_server.recvfrom(200)
                packet_new = packet_new[0]
                stop = 1
                print "got new packet!"
                unpacked = unpack('!fiiiii', packet_new[-24:])
                print "unpacked", unpacked
                # if not Q_reff_short.full():
                #     Q_reff_short.put(unpacked[5])
                #     print unpacked[5]

                # Save in DB
                Server_packet_received.snr_remote = unpacked[0]
                Server_packet_received.PLS = unpacked[1]
                Server_packet_received.freq_of_set = unpacked[2]
                Server_packet_received.time_of_set = unpacked[3]
                Server_packet_received.modem_state = unpacked[4]
                Server_packet_received.short_flag = unpacked[5]
                Server_packet_received.save()

            except Exception as e:
                print "no packet yet",e

            # Not needed with the new DB implementation
            if not Q_packet.full():
                Q_packet.put(packet_new)

            #time.sleep(15)
############################################################
def create_socket(sockip,sockport):

    try:
        sockname = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sockname.bind((sockip, sockport))
    except socket.error, msg:
        print "server :"
        print 'Socket could not be created.', str(msg[0]) + 'Message' + msg[1]
        sys.exit()
    return sockname

############################################################################################
#CLIENT
############################################################################################
interval = 0.5
name_list = []
value_list = []
i = 0
old_pls_client  = 156 # the default pls
################################################################################
##################################################################################
def command_and_repliy(ssh,command):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
    output = ssh_stdout.read()
    return output
def client_main():
    global switch
    thread_number = 0b00000010
    while switch_smart[5]:  ### new switch operation concept
        continue
    print "client started"
    reff_for_remote = 0
    root = tk.Tk()
    frame = window(root)
    frame.pack(fill = "both", expand = True)
    frame.add_lable("client")
    root.title("client")
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    rpi = {"username": modem_user, "password": modem_password, "hostname": modem_ip}
    try:
        ssh.connect(**rpi)
        print "connected"

    except:
        print "connection failed try again"

    while 1:
        if switch_smart[5] == 1:  ### new switch operation concept

            command = ('/mnt/flash1/demodstats.exe') #the demodstats command prints out importent info from the terminal such as snr and pls
            output=command_and_repliy(ssh,command)

            #putting the output in ds_table while splitting it to a list by each string
            ds_table = output.split()
            ds_len = len(ds_table)
            #defining the length of the for loop by the length of the list created by splitting the output
            for i in range(ds_len):
                #the list created contains values and their names one after the other, so by splliting it to even and odd(location wise) we can later form a dictionary
                if i % 2 != 0:

                    value_list.append(ds_table[i])

                else:

                    name_list.append(ds_table[i])
            #forming a dictionary bonding the names list and the values list
            ds_dict = dict(zip(name_list, value_list))
            #pulling the pls value from the dictionary by its name (hell yeah)
            try:
                pls = ds_dict["lastNonDummyPls"]
                #print pls !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! pleasssse put me back on
            except:
                print "no such value"

            #calculating the snr in dB after pulling it from the dictionary
            try:
                snrAvg = 10 * math.log10(int(ds_dict["snrAveraged"]) / (2 ** 14))
                #rounding the snr to .00 precision so i can work with a fixed value length
                snrAvg = round(snrAvg,2)
            #if the math doesnt addup it means that the snr is probably one or zero..anyway its no good for the calc and it scearms an error so we made this defalt snr value for whenever that happends
            except:
                snrAvg = -10
            #print "the current snr",  snrAvg
            time.sleep(interval)
            if not Q_reff.empty():
                #reff_for_remote = random.randint(15,25)
                reff_for_remote=copy.deepcopy(Q_reff.get())
            struct_packet_data = pack('!fiiiii', snrAvg,int(pls), int(ds_dict["freqOffset"]),int(ds_dict["timeOffset"]),int(ds_dict["modemState"]),reff_for_remote)
            unpacked = unpack('!fiiiii',struct_packet_data)
            #print "unpacked",unpacked
            txt = "unpacked struct",unpacked
            frame.add_txt(txt)
            try :
                #sending the structure with the list in udp
                #print struct_packet_data
                s.sendto(struct_packet_data, (remote_ip, udp_port))                    #

            except:
                print "could not send snr.."
            old_pls_client = pls

########################################################################
bufsize = 1
q_RRM = Queue.Queue(bufsize)
q_RRM_for_freq = Queue.Queue(bufsize)
########################################################################



#the use of Queue allows me to cross variables trough threads
def RRM_():
    global switch
    thread_number = 0b01000000
    while switch_smart[6] == 0:  ### new switch operation concept
        continue
    print "RRM stated"
    root = tk.Tk()
    frame = window(root)
    frame.pack(fill="both", expand=True)
    frame.add_lable("RRM")
    root.title("RRM")
    #s_Q = create_socket('10.0.1.48', 23507)
    s_Q = create_socket(my_ip, 23507)
    '''
    try:#creating socket on wifi
        s_Q = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s_Q.bind(('10.0.1.48', 23507))

    except socket.error, msg:
        print 'Socket could not be created.', str(msg[0]) + 'Message' + msg[1]
        sys.exit()
    '''
    s_Q.settimeout(5)
    while True:
        if switch_smart[6] == 1:  ### new switch operation concept

            try:#recieving the Q from the RRM (eli's laptop)
                new_q = s_Q.recvfrom(200)
                new_q = new_q[0]
                new_q = unpack('!i5f', new_q)
                maxPLS, txFreq, rxFreq, txSR, rxSr, margin = new_q
                zzz = [maxPLS,  txFreq, rxFreq, txSR, rxSr, margin]
                txt = "!!!!!!!!!!!!got eli",zzz
                frame.add_txt(txt)
                if not q_RRM.full():
                    q_RRM.put(zzz)
                else:
                    continue
                # new DB implementation: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                RRM_cfg_received.MaxPLS = maxPLS
                RRM_cfg_received.TxFreq = txFreq
                RRM_cfg_received.RxFreq = rxFreq
                RRM_cfg_received.TxSR = txSR
                RRM_cfg_received.RxSR = rxSr
                RRM_cfg_received.Margin = margin
                RRM_cfg_received.save()

                if not q_RRM_for_freq.full():
                    q_RRM_for_freq.put(zzz)
                else:
                    continue

            except Exception as e:
                #print(e)

                frame.add_txt("no messaege from RRM")

########################################################################
def datarate():
    global switch,ipRRM
    thread_number = 0b00001000
    while switch_smart[6]==0:  ### !!!same switch as RRM!!!
        continue
    root = tk.Tk()
    frame = window(root)
    frame.pack(fill="both", expand=True)
    frame.add_lable("dataRate")
    root.title("dataRate")
    ID =76
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    ni.ifaddresses('br0')
    #ipRRM = ni.ifaddresses('wlx90f652014f28')[ni.AF_INET][0]['addr']
    #ipRRM = '192.168.70.188'
    RRM_port = 12345
    while True:
        if switch_smart[6] == 1:  ### new switch operation concept
            # getting my IP address by the interface
            ni.ifaddresses('br0')
            ip = ni.ifaddresses('br0')[ni.AF_INET][0]['addr']
           # print ip
            # calculating the data rate
            x = psutil.net_io_counters(pernic=True)['br0'][0]
            time.sleep(1)
            x1 = psutil.net_io_counters(pernic=True)['br0'][0]
            diff = x1 - x
            txt="data for less sec", diff
            frame.add_txt(txt)
            # sendding the datarate and my ip as a structure
            time_sent = time.strftime("%Y-%m-%d %H: %M: %S",time.gmtime())
            txt= "time sent",time_sent
            frame.add_txt(txt)
            sending_struct = pack('!6i21s', diff, diff, diff, diff, diff, ID,time_sent)
            s.sendto(sending_struct, (ipRRM, RRM_port))  # it should be sent to the rrm
########################################################################
def short_corrector():
    global modem_ip,modem_password,modem_user,switch
    thread_number = 0b00010000
    while switch_smart[1] == 0:
        continue
    print "short started"
    flag_short =0
    flag =0

    stop =0
    snr_reff = 20 # has to be calculated
    root = tk.Tk()
    frame = window(root)
    frame.pack(fill="both", expand=True)
    frame.add_lable("short")
    root.title("short")
    ssh_short= paramiko.SSHClient()
    ssh_short.load_system_host_keys()
    ssh_short.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    rpi = {"username": modem_user, "password": modem_password, "hostname": modem_ip}

    try:
        ssh_short.connect(**rpi)
        print "connected"

    except:
        print "connection failed try again"

    demod_dict = demodstats(ssh_short)
    snr_reff = demod_dict['snrAveraged']

    try:
        snr_reff = 10 * math.log10(int(snr_reff) / (2 ** 14))
    except:
        print "error calc"
        snr_reff=0

    snr_short=snr_reff ##just got it for the first time from the demodstats

    while True:
        if switch_smart[1] == 1:  ### new switch operation concept
            if not Q_reff_short.empty():
                flag = copy.deepcopy(Q_reff_short.get())


        if thread_number & switch == thread_number:  ### new switch operation concept
            #if not Q_reff_short.empty():
            try:
                #flag = copy.deepcopy(Q_reff_short.get()) - - for new implementation with DB
                flag = Server_packet_received.short_flag
                if flag ==1 and flag_short ==0:
                    demod_dict = demodstats(ssh_short)  ###this is for working only when the flag is changed from 0 to 1
                    snr_reff = demod_dict['snr']
                    try:
                        snr_reff = 10 * math.log10(int(snr_reff) / (2 ** 14))
                    except:
                        print "error calc"
                        snr_reff=0
                    print '!!!!!!!!!@@@@@@@@@@@############$$$$$$$$$$$$$$$',snr_reff
                    flag_short = 1

                    delay(1)

                else:
                    flag_short = flag
            except:
                txt = ("No server_packet to take")
                frame.add_txt(txt)
            demod_dict = demodstats(ssh_short)
            snr_short = int(demod_dict['snrAveraged'])

            try:
                snr_short = 10 * math.log10(int(snr_short) / (2 ** 14))
            except:
                print "error calc"
            change = (snr_reff - snr_short)
            if abs(change) >=1 and flag ==0: ######the short loop will only work when the flag didnt point on an intended change and the change was bigger than 1dB

                txt = "snr reff: %d"%snr_reff
                frame.add_txt(txt)
                #frame.update_idletasks()
                #frame.update()
                #print "reff:",snr_reff
                demod_dict = demodstats(ssh_short)
                snr_short =  demod_dict['snrAveraged']

                try:
                    snr_short = 10 * math.log10(int(snr_short)/ (2 ** 14))
                except:
                    print "error calc"
                    snr_short=0
                txt = "snr = %f"%snr_short
                frame.add_txt(txt)
                delta_short = snr_reff - snr_short

                #while stop != 1:
                if not Q_delta.full() and flag_short !=1: # to avoid adding unnecessary delta
                    Q_delta.put(delta_short)
                #        stop = 1
                #else:
                #   stop = 0
                txt = "delta short: %f "%delta_short
                frame.add_txt(txt)
                time.sleep(0.5)
            else :
                txt = "not in working state"
                frame.add_txt(txt)
                txt = "change:",change
                frame.add_txt(txt)
                txt = "flag",flag
                frame.add_txt(txt)
                delay(0.5)
########################################################################
def SSH_Request(command):
    stop =0
    while stop ==0:
        if Q_RTOScommand.full():
            stop =1
            Q_RTOScommand.put(command)

########################################################################
def tx_command():
    global  switch
    thread_number = 0b00000001
    while switch_smart[3] == 0:  ### new switch operation concept
        continue
    print "tx started"
    time.sleep(0.2)
    old_sr = 15000000
    old_st = 15000000
    reff_freq = 10**6
    old_freq_tx = 1100000
    old_freq_rx = 1200000
    # tx_offset_sum = 0
    # rx_offset_sum = 0
    avg_list = [0]*10
    nothing_new =0
    change_reff_flag=0
    stop =0
    avg_tx =0
    delta_sum =0
    delta_l_sum = 0
    delta_l = 0
    corrector_l = 0
    delta_l_sum = 0
    root = tk.Tk()
    frame = window(root)
    frame.pack(fill="both", expand=True)
    frame.add_lable("tx command")
    root.title("tx command")
    current_Tx = -14#for starters
    old_tx = current_Tx
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    rpi = {"username": modem_user, "password": modem_password, "hostname": modem_ip}
    try:
        ssh.connect(**rpi)
        print "connected tx command"

    except:
        print "connection failed try again"

    # new DB implementation: for starters
    current_pls = Last_details.PLS

    # command = ('/tmp/RfTxLBandFreqWrite %d' % old_freq_tx)
    # ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
    # command = ('/tmp/RfTxDisableMute 0 0')
    # ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
    # command = ('/tmp/RfRxLBandFreqWrite %d' % old_freq_rx)
    # ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
    # command = ('/tmp/RfTxPowerCfg %d'%current_Tx)
    # ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
    # time.sleep(1)
    while True:
        if switch_smart[3] == 1: ### new switch operation concept
            # if not Q_delta_long.empty():
            #     delta_l = copy.deepcopy(Q_delta_long.get())
            #     txt = "delta long !!! ", delta_l
            #     frame.add_txt(txt)
            #     corrector_l = delta_l - delta_l_sum
            #     delta_l_sum = delta_l_sum + corrector_l
            #     current_Tx = current_Tx + corrector_l
            #
            #     if current_Tx >= 5:
            #         current_Tx = 5
            #
            #     if current_Tx <= -35:  ##this is for avoiding the non linear segment
            #         current_Tx = -35
            #
            #     command = ('/tmp/RfTxPowerCfg %d' % current_Tx)  # (current_Tx-1))#the (-1) is for calibration
            #     ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
            #     print "current tx after acm delta", current_Tx
            #     SSH_Request(('RfTxPowerCfg %d' % current_Tx))
            #
            #     nothing_new=1
            # else:
            #     nothing_new =0
            # new DB implementation: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            if Last_details.DeltaLong != 99:
                delta_l = Last_details.DeltaLong
                txt = "delta long !!! ", delta_l
                frame.add_txt(txt)
                corrector_l = delta_l - delta_l_sum
                delta_l_sum = delta_l_sum + corrector_l
                current_Tx = current_Tx + corrector_l

                if current_Tx >= 5:
                    current_Tx = 5

                if current_Tx <= -35:  ##this is for avoiding the non linear segment
                    current_Tx = -35

                command = ('/tmp/RfTxPowerCfg %d' % current_Tx)  # (current_Tx-1))#the (-1) is for calibration
                ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
                print "current tx after acm delta", current_Tx
                #SSH_Request(('RfTxPowerCfg %d' % current_Tx)) - - - - for freeRTOS implementation
                Last_details.DeltaLong = 99  # reset for next change
                Last_details.save()


            # if not Q_pls.empty():
            #     pls = copy.deepcopy(Q_pls.get())
            #     command = ('/mnt/flash1/txgse_setacm.exe -p %d' % (int(pls)))
            #     ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
            #     SSH_Request('')
            #     txt = "applied pls ",pls
            #     frame.add_txt(txt)
            # new DB implementation: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            if Last_details.PLS != current_pls:
                pls = Last_details.PLS
                command = ('/mnt/flash1/txgse_setacm.exe -p %d' % (int(pls)))
                ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
                SSH_Request('')
                txt = "applied pls ", pls
                frame.add_txt(txt)
                current_pls = Last_details.PLS

            if not q_RRM_for_freq.empty():
                freq_offset = copy.deepcopy(q_RRM_for_freq.get())
                freq_offset_tx = int(round(freq_offset[1],3)*10**3)
                #new_freq_tx = old_freq_tx + freq_offset_tx
                new_freq_tx = reff_freq + freq_offset_tx
                txt = "new freq tx!! = ",new_freq_tx
                frame.add_txt(txt)
                freq_offset_rx = int(round(freq_offset[2], 3) * 10**3)
                new_freq_rx = reff_freq + freq_offset_rx
                #new_freq_rx = old_freq_rx + freq_offset_rx
                txt = "new freq rx!! = ", new_freq_rx
                frame.add_txt(txt)

                if new_freq_tx != old_freq_tx:
                    time.sleep(1.5)
                    command = ('/tmp/RfTxLBandFreqWrite %d' % new_freq_tx)
                    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
                    command = ('/tmp/RfTxDisableMute 0 0')
                    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
                    old_freq_tx = new_freq_tx

                if new_freq_rx != old_freq_rx:
                    command = ('/tmp/RfTRxLBandFreqWrite %d' % new_freq_rx)
                    ssh.exec_command(command)
                    old_freq_rx = new_freq_rx
            # new DB implementation: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




            # if not Q_TX.full():
            #     Q_TX.put(current_Tx)
            # new DB implementation: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Last_details.TxPWR = current_Tx


            delta_for_remote_short = current_Tx-old_tx
            old_tx = current_Tx

            avg_list.pop(0)
            avg_list.insert(9, current_Tx)
            avg_tx = reduce(lambda x, y: x + y, avg_list) / len(avg_list)

            if old_tx + 0.7 >= avg_tx and old_tx - 0.7 <= avg_tx:

                change_reff_flag = 0

            else:
                if nothing_new ==1:
                    change_reff_flag = 1
                    old_tx = current_Tx

            while stop != 1:
                if not Q_reff.full():

                    Q_reff.put(change_reff_flag)
                    # txt = "added new delta reff for remote",delta_for_remote_short
                    stop = 1
                    # if change_reff_flag ==1:
                    #     delay(1)####creating a delay when the flag is '1' to reduce the amount of '1's sent when change happens
                else:
                    stop = 0

            stop = 0
            txt = "changed flag to", change_reff_flag
            frame.add_txt(txt)

            if change_reff_flag == 1:
                delay(1.5)

            command = ('/tmp/RfTxPowerCfg %d' % current_Tx)
            ssh.exec_command(command)

            if not Q_delta.empty():
                delta = copy.deepcopy(Q_delta.get())
                # delta = Q_delta.get()
                delta = round(delta, 2)
                corrector = delta - delta_sum
                txt = "delta_sum = %f" % delta_sum
                frame.add_txt(txt)
                delta_sum = delta_sum + corrector
                # if corrector < 10:
                current_Tx = current_Tx + corrector

                command = ('/tmp/RfTxPowerCfg %d' % current_Tx)
                ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
                # print "the delta",delta
                txt = "delta short:%f" % delta
                frame.add_txt(txt)
                time.sleep(0.1)
            if not Q_tx_freq.empty(): # when changing to db , add an 'if' that validates the new freq with the old one
                tx_freq_web = copy.deepcopy(Q_tx_freq.get())
                command = ('/tmp/RfTxLBandFreqWrite %d' % tx_freq_web)
                ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
                command = ('/tmp/RfTxDisableMute 0 0')
                ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
########################################################################
def demodstats(ssh) :
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('/mnt/flash1/demodstats.exe')
    demod = ssh_stdout.read()
    demod = demod.split()
    ds_len = len(demod)
    # defining the length of the for loop by the length of the list created by splitting the output
    for i in range(ds_len):
        # the list created contains values and their names one after the other, so by splliting it to even and odd(location wise) we can later form a dictionary
        if i % 2 != 0:

            value_list.append(demod[i])

        else:

            name_list.append(demod[i])
    # forming a dictionary bonding the names list and the values list
    demod_dict = dict(zip(name_list, value_list))
    return demod_dict
########################################################################
def new_ACM():
    time.sleep(1)
    global switch,switch_smart
    thread_number = 0b00100000
    print "acm started"
    global pls_data_list
    # global snr
    global current_pls
    global counter
    global pls_max
    global pls_min
    global snr_dict
    global tx_min
    global my_ip
    global udp_port_server
    global tx_max_real
    no_pls = 0
    counter_acm =0
    Reff_snr = 11
    current_Tx = -14 #because this is whrere the tx controllet starts from
    pls_target =pls_max
    pls_max_real = pls_max #for starters
    delta_long = 0
    stop = 0
    counter = 0
    new_max_pls = 0
    root_gain =tk.Tk()
    frame_gain = window(root_gain)
    frame_gain.pack(fill="both",expand = True)
    frame_gain.add_lable("gain")
    root_gain.title("gain")
    while switch_smart[0] == 0:
        continue

    root = tk.Tk()
    frame = window(root)
    frame.pack(fill="both", expand=True)
    frame.add_lable("ACM")
    root.title("ACM")
    ssh = ssh_connection()

    command = ('/mnt/flash1/txgse_setacm.exe -p %d')% Last_details.PLS
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
    delay(1)

    while True:
        if switch_smart[0] == 1:
            if Server_packet_received.PLS != -1:
                # while stop !=1:
                #     if not Q_TX.empty():
                #         current_Tx = copy.deepcopy(Q_TX.get())
                #         stop = 1
                #     else :
                #         stop = 0
                # stop = 0
                # new DB implementation: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                current_Tx = Last_details.TxPWR

                # if not q_RRM.empty():
                #     no_pls = 1
                #     qVar = copy.deepcopy(q_RRM.get())
                #     pls_target = qVar[0]
                #     txt = "pls target from RRM", pls_target
                #     frame.add_txt(txt)
                #     print "pls target from RRM",pls_target
                # else :
                #     no_pls = 0
                # new DB implementation: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                try:
                    pls_target = RRM_cfg_received.MaxPLS
                except:
                    print "no configurations from RRM"

                try:
                    snr = Server_packet_received.snr_remote
                    current_pls = Server_packet_received.PLS
                    print snr, current_pls
                    Reff_snr = snr
                except:
                    print "server doesnt work"

                print "new snr reference =",Reff_snr

                #packet = connection_sensor_smart(s_server)

                if  (pls_target <= 214 and pls_target >= 132): # if we get a wrong number for any reason we'll skip the rest..

                    ###ACM
                    # extracting an 'only snr' table to find the nearest pls
                    current_max_snr = (tx_max_real - current_Tx) + snr # calculating the remaining gain and adding it to the current snr
                    #current_max_snr = math.ceil(current_max_snr) # always rounds up
                    current_max_snr = round(current_max_snr,0) # always rounds down
                    txt = "current max snr = ",current_max_snr
                    frame_gain.add_txt(txt)
                    modcod_params = MCTable().findByEsNo(current_max_snr)

                    print "modcod parameters", modcod_params
                    pls_max_real= modcod_params[1]
                    print "pls max by calc", pls_max_real


                    if pls_max_real >= pls_target:
                        pls_data_list = snr_target_modcod(pls_target)  #extracting the relevant snr for the pls
                    else:
                        txt = "pls %d is higher then max, changing to "%pls_target, pls_max_real
                        frame.add_txt(txt)
                        pls_data_list = snr_target_modcod(pls_max_real)
                        pls_target = pls_max_real

                    pls_data_list = snr_target_modcod(pls_target)
                    snr_QF = pls_data_list[0]
                    target_snr = snr_QF + merge #margin
                    txt = "snr target=====", target_snr
                    frame.add_txt(txt)
                    print "snr target=====", target_snr
                    #delta_long = target_snr - snr
                    #txt = "delta long =====", delta_long
                    #frame.add_txt(txt)


                    delta_long =(target_snr-Reff_snr)
                    txt = "delta acm =",delta_long
                    frame.add_txt(txt)
                    print "delta acm =",delta_long
                    if abs(delta_long) >=1 and switch_list[7] & switch == 1: ## the switch is for ULPC control
                        if not Q_delta_long.full() :
                            #if (target_snr >= snr and target_snr - 0.5 <= snr): # dont forget! target_snr = snr + margin //already added uper border
                            Q_delta_long.put(delta_long)
                            delta_long = 0
                        # new DB implementation: ~~~~~~~~~~~~~~~~~~~~
                    Last_details.DeltaLong = delta_long

                    if current_pls != pls_target:
                        if not Q_pls.full():
                            Q_pls.put(int(pls_target))
                        # new DB implementation:  ~~~~~~~~~~~~~~~
                        Last_details.PLS = pls_target

                    Last_details.save()
                        #command = ('/mnt/flash1/txgse_setacm.exe -p %d' % (int(pls_target)))
                        #ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
                    time.sleep(1)

                else :
                    print "target pls not valid",pls_target
                    time.sleep(2)

            else:
                print "No packet not working!"
                time.sleep(0.5)


############################################################################################
def find_ip():
    global my_ip,ID,last_rf_cfg
    # getting my IP address by the interface
    try:
        ni.ifaddresses('enp0s25')
        my_ip = ni.ifaddresses('enp0s25')[ni.AF_INET][0]['addr']
        last_rf_cfg['my IP'] = my_ip
        ID = my_ip.split('.')
        ID = ID[3]
        print ID
        print "this is my ip",my_ip
        Last_details.my_IP = my_ip  # Change it in the DB
        Last_details.save()
    except Exception as e :
        print "couldnt find ip",e
#
# def switch_Tx_on():
#     global switch
#     x = switch_list[0]
#     switch = switch | x
#
# def switch_Tx_off():
#     global switch
#     x = switch_list[0]
#     x = ~x
#     switch = switch & x
#
# def switch_client_on():
#     global switch
#     x = switch_list[1]
#     switch = switch | x
#
# def switch_client_off():
#     global switch
#     x = switch_list[1]
#     x = ~x
#     switch = switch & x
#
# def switch_server_on():
#     global switch
#     x = switch_list[2]
#     switch = switch | x
#
# def switch_server_off():
#     global switch
#     x = switch_list[2]
#     x = ~x
#     switch = switch & x
#
# def switch_data_on():
#     global switch
#     x = switch_list[3]
#     switch = switch | x
#
# def switch_data_off():
#     global switch
#     x = switch_list[3]
#     x = ~x
#     switch = switch & x
#
# def switch_short_on():
#     global switch
#     x = switch_list[4]
#     switch = switch | x
#
# def switch_short_off():
#     global switch
#     x = switch_list[4]
#     x = ~x
#     switch = switch & x
#
# # def switch_acm_on():
# #     global switch
# #     x = switch_list[5]
# #     switch = switch | x
# #
# # def switch_acm_off():
# #     global switch
# #     x = switch_list[5]
# #     x = ~x
# #     switch = switch & x
#
# def switch_rrm_on():
#     global switch
#     x = switch_list[6]
#     switch = switch | x
#
# def switch_rrm_off():
#     global switch
#     x = switch_list[6]
#     x = ~x
#     switch = switch & x
#
# def switch_ULPC_on():
#     global switch
#     x = switch_list[7]
#     switch = switch | x
#
# def switch_ULPC_off():
#     global switch
#     x = switch_list[7]
#     x = ~x
#     switch = switch & x



########################################################################
from flask import Flask
from flask import render_template
from flask import request
import os
import subprocess
from pymongo import MongoClient
from mongoengine import *

app = Flask(__name__)

'''
    New integration with MongoDB
'''

# Connection to local MongoDB:
MongoClient('localhost', 27017)

# Create / connect the DB:
db = connect('newtest')


class LastRFCfg(Document):
    TxSR = IntField(default=15)
    RxSR = IntField(default=15)
    tx_freq = IntField(default=1100)
    rx_freq = IntField(default=1200)
    my_IP = StringField()  # need to get from project - my_ip
    RRM_IP = StringField()  # need to get from project - ipRRM
    modem_state = IntField(default=0)
    PING = IntField(default=0)
    SNR= IntField(default=0)
    RxPWR = StringField(default='non')
    udp_port = IntField()  # need to get from project - udp_port
    counter = IntField(default=0)
    TxPWR = IntField(default=0)
    PLS = IntField(default=154)
    TxCW = StringField(default='off')  # can be boolean?
    TxOnOff = IntField(default=0)
    TxBucPower = IntField(default=0)
    RxLNBPower = IntField(default=0)
    RemoteTermIP = StringField()  # need to get from project - remote_ip
    TermIP = StringField()  # need to get from project - my_ip
    ACMSwitch = IntField(default=0)
    ShortSwitch = IntField(default=0)
    ULPCSwitch = IntField(default=0)
    P1dBSwitch = IntField(default=0)
    RRMSwitch = IntField(default=0)
    TxCNTRL = IntField(default=0)
    Clientswitch = IntField(default=0)
    TxMaxPWR = IntField(default=5)  # should come from 1dBcp
    MaxPLS = IntField(default=178)  # should come from RRM
    DeltaLong = FloatField(default=99)  # default says no change

# Creating an object with the current status:
Last_details = LastRFCfg()
Last_details.my_IP = my_ip
Last_details.RRM_IP = ipRRM
Last_details.udp_port = udp_port
Last_details.RemoteTermIP = remote_ip
Last_details.save()


class ServerPacket(Document):
    freq_of_set = IntField()
    time_of_set = IntField()
    PLS = IntField(default=-1)  # default says no change
    modem_state = IntField()
    short_flag = IntField()
    snr_remote = IntField()

Server_packet_received = ServerPacket()
Server_packet_received.save()


class RRMCfg(Document):
    MaxPLS = IntField(default= 214)
    TxFreq = IntField()
    RxFreq = IntField()
    TxSR = IntField()
    RxSR = IntField()
    Margin = IntField()

RRM_cfg_received = RRMCfg()
RRM_cfg_received.save()


class MCTable:
    '''
    ModCod table class - holds the info about the modcods, eff, esno
    also equipped with usefull functions which are used in other classes
    '''

    def __init__(self, sw=None):
        self.sw = sw
        self.pls = [132, 134, 136,
                    138, 140,
                    148, 150, 152, 158, 160, 164, 166, 168, 170,
                    174, 178, 180, 182,
                    184, 186, 190, 194, 198,
                    204, 206, 208, 210, 212, 214]
        self.ModCod = ["QPSK 13/45", "QPSK 9/20", "QPSK 11/20",
                       "8APSK 5/9-L", "8APSK 26/45-L",
                       "16APSK 1/2-L", "16APSK 8/15-L", "16APSK 5/9-L", "16APSK 3/5-L", "16APSK 28/45", "16APSK 2/3-L",
                       "16APSK 25/36", "16APSK 13/18", "16APSK 7/9",
                       "32APSK 2/3-L", "32APSK 32/45", "32APSK 11/15", "32APSK 7/9",
                       "64APSK 32/45-L", "64APSK 11/15", "64APSK 7/9", "64APSK 4/5", "64APSK 5/6",
                       "256APSK 29/45-L", "256APSK 2/3-L", "256APSK 31/45-L", "256APSK 32/45", "256APSK 11/15-L",
                       "256APSK 3/4"]
        self.SpecEff = [0.567805, 0.889135, 1.088581,
                        1.647211, 1.713601,
                        1.972253, 2.104850, 2.193247, 2.370043, 2.458441, 2.635236, 2.745734, 2.856231, 3.077225,
                        3.289502, 3.510192, 3.620536, 3.841226,
                        4.206428, 4.338659, 4.603122, 4.735354, 4.933701,
                        5.065690, 5.241514, 5.417338, 5.593162, 5.768987, 5.900855]
        self.EsNo = [-2.03, 0.22, 1.45,
                     4.73, 5.13,
                     5.97, 6.55, 6.84, 7.41, 8.1, 8.43, 9.27, 9.71, 10.65,
                     11.1, 11.75, 12.17, 13.05,
                     13.98, 14.81, 15.47, 15.87, 16.55,
                     16.98, 17.24, 18.1, 18.59, 18.84, 19.57]

    def find_nearest(self, array, value):
        '''
        searching for the NOT ACCURATE match in the discrete range
        :param array: the array to search in
        :param value: the value to search for in array
        :return: the index of the closest number to the searched
        '''
        x = np.abs(np.array(array) - value)
        idx = np.where(x == x.min())
        return idx[0]

    def findByEff(self, eff):
        '''
        search for the various parameters in the array which match to eff
        :param eff: the effeciency to search for
        :return: closest effeciency, PLS matching the effeciency, ModCod name, Es/No required for the effeciency
        '''
        idxEff = self.find_nearest(self.SpecEff, eff)
        if len(idxEff) > 1:
            idx = int(idxEff[0])
        else:
            idx = int(idxEff)
        if idx == 0:
            pass

        else:
            if self.SpecEff[idx] > eff:
                idx = int(idx - 1)
        return self.SpecEff[idx], self.pls[idx], self.ModCod[idx], self.EsNo[idx]

    def findByEsNo(self, esno):
        '''
        search for the various parameters in the array which match to esno
        :param esno: the esno to search for
        :return: closest effeciency, PLS matching the effeciency, ModCod name, Es/No required for the effeciency
        '''
        idxEff = self.find_nearest(self.EsNo, esno)
        if len(idxEff) > 1:
            idx = int(idxEff[0])
        else:
            idx = int(idxEff)
        if idx == 0:
            pass
            # print 'lowest ModCod chosen'
        else:
            if self.EsNo[idx] > esno:
                idx = int(idx - 1)

        return self.SpecEff[idx], self.pls[idx], self.ModCod[idx], self.EsNo[idx]

    def findByPLS(self, pls):
        '''
        search for the various parameters in the array which match to PLS
        :param pls: the PLS to search for
        :return: closest effeciency, PLS matching the effeciency, ModCod name, Es/No required for the effeciency
        '''
        idxEff = self.find_nearest(self.pls, pls)
        if len(idxEff) > 1:
            idx = int(idxEff[0])
        else:
            idx = int(idxEff)
        print 'pls', pls, 'PLS', self.pls[idx], 'ModCod', self.ModCod[idx], 'Es/No', self.EsNo[idx], 'spec eff', \
            self.SpecEff[idx]
        return self.pls[idx]


last_rf_cfg ={'simbol rate Tx':15,
              'RxSR':15,
              'tx freq [MHz]':1100,
              'rx freq [MHz]':1200,
              'my IP':my_ip,
              'IP RRM': ipRRM,
              'modem state': 0,
              'PING':0,
              'SNR':0,
              'Rx Power':'non',
              'UDP port':udp_port,
              'counter':0,
              'TxPWR':0,
              'PLS':154,
              'TxCW':'off',
              'TxOnOff':0,
              'TxBucPower':0,
              'RxLNBPower':0,
              'RemoteTermIP':remote_ip,
              'TermIP':my_ip,
              'RRMIP':ipRRM,
              'ACMSwitch':0,
              'ShortSwitch':0,
              'ULPCSwitch':0,
              'P1dBSwitch':0,
              'RRMSwitch':0,
              'TxCNTRL':0,
              'Clientswitch':0
              }
snr_port = 12345
# s_snr = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
# s_snr.bind((my_static_ip,snr_port))

@app.route('/SCPC', methods=['GET', 'POST'])
def post_json():
    if request.method == 'POST':
        print 'hoorey'
    post = request.get_json()
    print post
    # param = post.get('RxSR')
    # print param


@app.route('/control', methods=['GET', 'POST'])
def control():
    SCPCMain()
    result = 0
    return render_template('index2.html', result=result, error=error, stats=last_rf_cfg)


@app.route('/demo', methods=['GET', 'POST'])
def demo():
    SCPCMain()
    result = 0
    #  TO DO - query the DB directly from web (node js / js)
    return render_template('simple.html', result=result, error=error,
                           TxSR=Last_details.TxSR,
                           RxSR=Last_details.RxSR,
                           tx_freq=Last_details.tx_freq,
                           rx_freq=Last_details.rx_freq,
                           PLS=Last_details.PLS,
                           my_IP=Last_details.my_IP,
                           RRM_IP=Last_details.RRM_IP,
                           RemoteTermIP=Last_details.RemoteTermIP,
                           udp_port=Last_details.udp_port,
                           PING=Last_details.PING,
                           clientSwitch=switch_smart[5],
                           serverSwitch=switch_smart[4],
                           acmSwitch=switch_smart[0],
                           txSwitch=switch_smart[3]
                           )


def SCPCMain():
    snr_interval = 1000
    global ipRRM,my_ip,remote_ip,last_rf_cfg,s_snr,udp_port_client,udp_port_server
    # last_rf_cfg ={'st':15*10**6 , 'sr':15*10**6,'pls':154,'tx freq':1100000,'rx freq':1200000,'my IP':my_ip, 'IP RRM': ipRRM,'modem state': 0}
    ssh = ssh_connection()
    # command = ('/mnt/flash1/pidof udp_snravg_client.exe %s %d %d 0 &'%(my_ip,snr_port,snr_interval))
    # ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)

    result = 0
    error = ''
    stats = {}
    # you may want to customize your GET... in this case not applicable
    if request.method == 'POST':


        # get the form data
        RxFreq = request.form['RxFreq']
        RxSR = request.form['RxSR']
        TxFreq = request.form['TxFreq']
        TxSR = request.form['TxSR']
        TxPWR = request.form['TxPWR']
        TxPLS = request.form['TxPLS']
        TxCW = request.form['TxCW']
        TxOnOff = request.form['TxOnOff']
        TxBucPower = request.form['TxBucPower']
        RxLNBPower = request.form['RxLNBPower']
        #RemoteTermIP = request.form['RemoteTermIP'] - - -because of demo
        #RECV_PORT = request.form['recv_port'] - - - because of demo
        #TRANS_PORT = request.form['trans_port'] - - - because of demo
        #TermIP = request.form['TermIP'] - - - because of demo
        #RRM_IP = request.form['RRMIP'] - - - because of demo
        PING = request.form['PING']
        ACMSwitch = request.form['ACMSwitch']
        ShortSwitch = request.form['ShortSwitch']
        ULPCSwitch = request.form['ULPCSwitch']
        P1dBSwitch = request.form['P1dBSwitch']
        RRMSwitch = request.form['RRMSwitch']
        TxCNTRL = request.form['TxCNTRL']
        Clientswitch = request.form['Client']
        SERVERswitch = request.form['SERVER']

        # print RxFreq,RxSR,TxFreq,TxSR,TxPWR,TxPLS,TxCW,TermIP,RRMIP,ACMSwitch,ShortSwitch,ULPCSwitch,P1dBSwitch,RRMSwitch

        # if RRMIP :  - - - because of demo
        #     ipRRM = RRMIP
        #     print "new RRM IP ",ipRRM
        #     Last_details.RRMIP = RRMIP

        # if RemoteTermIP :  - - - because of demo
        #     remote_ip = RemoteTermIP
        #     print "new remote terminal IP ",remote_ip
        #     Last_details.RemoteTermIP = RemoteTermIP

        # if RECV_PORT :  - - - because of demo
        #     udp_port_server = int(RECV_PORT)
        #     print "new server port",RECV_PORT
        #     last_rf_cfg['server port'] = udp_port_server
        #     Last_details.server_port = int(RECV_PORT)


        # if TRANS_PORT :  - - - because of demo
        #     udp_port_client = int(TRANS_PORT)
        #     print "new client port",TRANS_PORT
        #     last_rf_cfg['client port'] = udp_port_client
        #     Last_details.client_port = int(TRANS_PORT)

        if PING:
            with open(os.devnull, 'w') as DEVNULL:
                try:
                    subprocess.check_call(
                        ['ping', '-c', '1', remote_ip],
                        stdout=DEVNULL,  # suppress output
                        stderr=DEVNULL
                    )
                    is_up = True
                    last_rf_cfg['PING'] = 'got ping response from %s !' % remote_ip
                    Last_details.PING = 1
                except subprocess.CalledProcessError:
                    last_rf_cfg['PING'] = 'no ping to %s' % remote_ip
                    is_up = False
                    Last_details.PING = 0

            # response = os.system("ping -c 1 " + remote_ip)
            # # and then check the response...
            # if response == 0:
            #     last_rf_cfg['PING :']='got ping response from %s !' % remote_ip
            #     #print 'got ping response from ',remote_ip,' !'
            # else:
            #     last_rf_cfg['PING :'] = 'no ping to %s'%remote_ip
            #     #print 'no ping to %s'%remote_ip

        if RxFreq:
            print RxFreq
            #RxFreq = int(float(RxFreq) * 10 ** 3)
            command = ('/tmp/RfRxLBandFreqWrite %d' % int(float(RxFreq) * 10 ** 3))
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
            last_rf_cfg['rx freq [MHz]']= RxFreq
            Last_details.rx_freq = RxFreq
            time.sleep(0.1)

        if RxSR:
            RxSR = int(float(RxSR))#*10**6)
            last_rf_cfg['simbol rate Rx']=RxSR
            print RxSR
            Last_details.RxSR = RxSR
            Last_details.save()

        if TxFreq:
            print TxFreq
            #Txhttp://127.0.0.1:1234/Freq = int(float(TxFreq )* 10**3)
            command = ('/tmp/RfTxLBandFreqWrite %d' % int(float(TxFreq) * 10 ** 3))  # the command is in KHz
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
            time.sleep(0.1)
            command = ('/tmp/RfTxDisableMute 0 0')
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
            last_rf_cfg['tx freq [MHz]']= TxFreq
            Last_details.tx_freq = TxFreq

        if TxSR:
            TxSR = int(float(TxSR))# * 10 ** 6)
            last_rf_cfg['simbol rate Tx'] = TxSR
            print TxSR
            Last_details.TxSR = TxSR

        if TxPWR:
            print TxPWR
            TxPWR = float(TxPWR)
            if TxPWR >= -35 and TxPWR <= 5:
                command = ('/tmp/RfTxPowerCfg %d' % TxPWR)
                ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
                Last_details.TxPWR = TxPWR
            else :
                error = "wrong power (-35 to +5)"

        if TxPLS:
            TxPLS = int(TxPLS)
            last_rf_cfg['PLS'] = TxPLS
            print TxPLS
            Last_details.PLS = TxPLS
            Last_details.save()

        if TxCW:
            print TxCW
            # Can be boolean?

        # if TermIP:  - - - because of demo
        #     print TermIP
        # if RRMIP:  # Duplicate
        #     print RRMIP

        if TxOnOff=='1':
            print TxOnOff
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('/tmp/RfTxDisableMute 0 0')
            Last_details.TxOnOff = 1
        elif TxOnOff=='0':
            print TxOnOff
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('/tmp/RfTxDisableMute 1 0')
            Last_details.TxOnOff = 0

        if ACMSwitch=='1':
            switch_smart[0] = 1
            #switch_acm_on()
            #print ACMSwitch
        elif ACMSwitch!='1':
            switch_smart[0] = 0
            #switch_acm_off()
            #print ACMSwitch

        if ShortSwitch=='1':
            switch_smart[1] = 1
            # switch_short_on()
            print ShortSwitch
        elif ShortSwitch!='1':
            switch_smart[1] = 0
            # switch_short_off()
            print ShortSwitch

        if ULPCSwitch=='1':
            switch_smart[2] = 1
            print ULPCSwitch

        if P1dBSwitch=='1':
            switch_smart[2] = 0
            print P1dBSwitch

        if TxCNTRL=='1':
            switch_smart[3] = 1
            # switch_Tx_on()
            print TxCNTRL
        elif TxCNTRL =='0':
            switch_smart[3] = 0
            # switch_Tx_off()
            print TxCNTRL

        if SERVERswitch =='1':
            switch_smart[4] = 1
            # switch_server_on()
            print SERVERswitch
        elif SERVERswitch =='0':
            switch_smart[4] = 0
            # switch_server_off()
            print SERVERswitch

        if Clientswitch=='1':
            switch_smart[5] = 1
            # switch_client_on()
            # print Clientswitch
        elif Clientswitch =='0':
            switch_smart[5] = 0
            # switch_client_off()
            # print Clientswitch


        if RRMSwitch=='1':
            switch_smart[6] = 1
            # switch_rrm_on()
            print RRMSwitch
        elif RRMSwitch !='1':
            switch_smart[6] = 0
            # switch_rrm_off()
            print RRMSwitch
        if TxSR or RxSR and TxPLS:
            command = ('/mnt/flash1/sx3000b.exe -d gse --rh 4 --gf 1 --st %d --sr %d -p %d -t s2x' %((Last_details.TxSR*10**6),(Last_details.RxSR*10**6),Last_details.PLS))
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
        elif TxSR == "" and RxSR == "" and TxPLS:
            print 'only pls'
            command = ('/mnt/flash1/txgse_setacm.exe -p %d' %Last_details.PLS)
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)


        print "switch changed!!! new switch = ",bin(switch)
        command = ('/mnt/flash1/demodstats.exe | grep modemState')
        modem_stat = command_and_repliy(ssh,command)
        modem_stat = modem_stat.split()
        last_rf_cfg['modem state']=modem_stat[1]
        command = ('/mnt/flash1/udp_snravg_client.exe %s %d %d 0 &' % ('192.16.1.1', 12345, 100))
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
        try:
            pack = s_snr.recvfrom(200)
            pack = pack[0]
            pack = pack.split("'")

            snr_raw = pack[1]
            snr = 10 * math.log10(int(snr_raw) / (2 ** 14))
            print "snr averaged", snr
        except:
            snr = 0
        last_rf_cfg['SNR']=snr
        command = ('pidof udp_snravg_client.exe ')
        psid = command_and_repliy(ssh, command)
        command = ('kill %s' % psid)  # to stop yanivs snr from running
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
        command = ('/tmp/RfRxGetPower')
        RxPower = command_and_repliy(ssh, command)
        RxPower = RxPower.split()
        RxPower = RxPower[11]
        last_rf_cfg['Rx Power']=RxPower
        ssh.close()
        Last_details.save()

    #find_ip()
    last_rf_cfg['my IP'] = my_ip
    ssh.close()
    #return render_template('index2.html', result=result, error=error, stats=last_rf_cfg)
    #return jsonify({'static ': last_rf_cfg})




# if __name__ == "__main__":
#     app.run(host='0.0.0.0', port=1234)  # , debug=True)
###################################################################################
# @app.route('/')
# def index ():
#     return render_template('index.html')
# # def hello_world():
# #     return 'hello world'
###################################################################################
from flask import jsonify, send_from_directory
@app.after_request
def apply_caching(response):
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Headers"] = "*"
    return response
@app.route('/', methods =['POST','GET'])
def index ():
    post = request.get_json()
    return send_from_directory(app.root_path + '/app/', 'index.html')

@app.route('/<path:filename>')
def base_static(filename):
    return send_from_directory(app.root_path + '/app/', filename)

@app.route('/json' , methods = ['POST','GET'])
def jsonexample ():
    post = request.get_json()
    # while True :
    # time.sleep(1)
    last_rf_cfg['counter'] =last_rf_cfg['counter'] +1
    # return jsonify({'stats':last_rf_cfg})
    resp = jsonify({'stats': last_rf_cfg})
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

@app.route('/_stuff', methods= ['GET'])
def stuff():
    time.sleep(1)
    cpu=round(random())
    ram=round(random())
    disk=round(random())
    print cpu ,ram ,disk
    return jsonify(cpu=cpu, ram=ram, disk=disk)

########################################################################
def main():
    global switch
    print "Starting!!"
    switch = 0b00000000
    find_ip()
    c = threading.Thread(target=client_main)
    RRM = threading.Thread(target=RRM_)
    D = threading.Thread(target=datarate)
    short =threading.Thread(target=short_corrector)
    Tx = threading.Thread(target=tx_command)
    acm = threading.Thread(target=new_ACM)
    Server_ = threading.Thread(target=server)


    Tx.start()       # 00000001
    time.sleep(1)
    c.start()        # 00000010
    Server_.start()  # 00000100
    time.sleep(2)
    D.start()        # 00001000
    short.start()    # 00010000
    acm.start()      # 00100000
    time.sleep(2)
    RRM.start()      # 01000000

    app.run(host='0.0.0.0', port=1234)  # , debug=True)
    while True:

        # x = input("switch = ")
        # switch = int(str(x), 2)
        print switch

        # master = tk.Tk()
        # master.title("thread master")
        #
        #
        # tx_on = tk.Button(master,bg= 'green', text="Tx ON", command=(switch_Tx_on))
        # tx_on.pack()
        # tx_off = tk.Button(master,bg= 'orange', text="Tx OFF", command=(switch_Tx_off))
        # tx_off.pack()
        # client_on = tk.Button(master,bg= 'green', text="client ON", command=(switch_client_on))
        # client_on.pack()
        # client_off = tk.Button(master,bg= 'orange', text="client OFF", command=(switch_client_off))
        # client_off.pack()
        # server_on = tk.Button(master,bg= 'green', text="server ON", command=(switch_server_on))
        # server_on.pack()
        # server_off = tk.Button(master,bg= 'orange', text="server OFF", command=(switch_server_off))
        # server_off.pack()
        # data_on = tk.Button(master,bg= 'green', text="datarate ON", command=(switch_data_on))
        # data_on.pack()
        # data_off = tk.Button(master,bg= 'orange', text="datarate OFF", command=(switch_data_off))
        # data_off.pack()
        # short_on = tk.Button(master,bg= 'green', text="short ON", command=(switch_short_on))
        # short_on.pack()
        # short_off = tk.Button(master,bg= 'orange', text="short OFF", command=(switch_short_off))
        # short_off.pack()
        # acm_on = tk.Button(master,bg= 'green', text="acm ON", command=(switch_acm_on))
        # acm_on.pack()
        # acm_off = tk.Button(master,bg= 'orange', text="acm OFF", command=(switch_acm_off))
        # acm_off.pack()
        # rrm_on = tk.Button(master,bg= 'green', text="RRM ON", command=(switch_rrm_on))
        # rrm_on.pack()
        # rrm_off = tk.Button(master,bg= 'orange', text="RRM OFF", command=(switch_rrm_off))
        # rrm_off.pack()

        # master.mainloop()
if __name__ == '__main__':
    main()


